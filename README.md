# Notice (2019-11-23)
I'm pretty much abandoning gitlab for hosting this because they are doing stupid
anti-user things. Future updates will happen at [gitgud].


[gitgud]:https://gitgud.io/Wazubaba/nim-mangaupdater

# Notice (2019-06-29)
It seems like the manga-py dev intends to essentially create the very
functionality found in this tool within their version 2.0 milestone, so
this project may very well become defunct around that time. Depends on
how well theirs works compared to mine. I personally use this tool so
I'll be trying to at least keep it working till then, though adding new
features is not a high priority.

## Introduction
This tool is designed to let you basically automate [manga-py]. It is intended
to be used via a cron daemon and is not *quite* fully feature-complete but
good enough to be used right now.

I cannot be absolutely certain whether this will work on Windows or not,
but I can guarantee that currently you will have to run as admin because
the downloading functionality leverages symlinks to avoid excessive disk
I/O for working around short-comings in `manga-py`, which as far as I've
gathered for some obtuse reason Windows restricts this functionality to
admins. IDK if there is a feasible work-around for this, and frankly I
don't use Windows so it isn't really high on my priority lists. Yes
lists.

Also another caveat off the top of my head - you'll have to be careful
to rename your paths properly. Windows might go schizoid if you try to
feed it the monstrosities that are manga names as file paths. YMMV idk.

[manga-py]:https://manga-py.com/

## Usage
Please see [USAGE](doc/USAGE.md) for further information.

## Compiling
### Dependencies
You require the following deps (and obviously their sub-deps):
* [nim][1] (I developed with 1.0.2)
* nimble (This should be included with modern nim)
* [git][2] with support for submodules (I have yet to see a git package without this thankfully...)
* [python][3] 3.5 - 3.7 (according to manga-py)
* pip for your python version. This should either be included with python or you can install it from your package manager on linux.

To build you just need to invoke `nim build` in the project's root directory.
This will handle grabbing a confirmed working version of manga-py and it's deps
 - installing this all locally via python's setup thingy subsystem, grabbing the
nim deps you need, and then building everything in release mode with all
optimizations, also stripping the binaries if a strip executable can be found
on your PATH, though it is setup to invoke it like the standard strip program
found on linux.

[1]: https://nim-lang.org
[2]: https://git-scm.com/
[3]: https://www.python.org/


## Updating manga-py
Currently the submodule for manga-py is included because this version was confirmed
to be working without bugs, but due to the nature of web devs always having to
bloody tinker it will likely require updates periodically to handle changes to
the various manga sites. This can be done normally via pip iirc, or you can
manually update the submodule via just changing into it and running `git pull`.
After that you can just go back to the project root and invoke
`nim setup_mangapy`.


## Installing
Just copy the binaries to somewhere on your path.
Some tools depend on others, but they can optionally look for environment
variables to find the software if needed.

Currently the only tool that does this is `repl`. It will attempt to find
`updater` within your path, and failing that will check the `MANGA_UPDATER`
environment variable. If it cannot find it then the `download` command will
not function, though you can still manually invoke `updater` yourself if
necessary.

