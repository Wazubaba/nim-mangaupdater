The repl system is designed to make it really easy to interactively
add new entries to your list (think like when you are in the middle
of perusing or something). There are several commands you can use,
though be careful that if it isn't registered as a command then
repl will assume you are trying to enter a new manga.

All commands can be typed in part, so long as they can be resolved to
a valid argument. This means that if you type 'h', it will be seen as
'help'.

To add a new manga to your list, paste the name of the manga, followed
by a space, and finally the url of the manga.

ex:
	my manga here yes it has spaces https://some/url/thingy


Commands:
	help - show this help
	quit - quit and save your config
	save - save the config as it is
	info - show info on the current database
	download - save the config as it is, and then invoke updater
	base [path] - make a new list in memory which only has the
	configuration from the target list, and an empty manga list.

Notes:
Please bear in mind - repl is single-threaded - that is to say when
you run the download command you will have to wait for the download
to complete before you can actually continue. Don't mash keys or
hit enter a bunch or you might actually mess up your list.
