import strutils

import result
export result.isErr, result.ok, result.value, Result

#import config

type
  Container* = Result[tuple[name, url: string], Errors]
  Errors* = enum
    too_many_args,
    not_enough_args,
    malformed_args,
    reversed_args

proc split_name_url*(line: string): Container =
  let parsed = line.rsplit(" ", 1)
  if parsed.len() < 2:
    return Container.err(not_enough_args)
  elif parsed.len() > 2:
    return Container.err(too_many_args)
  else:
    # Test if it looks like the args are reversed
    if parsed[0].startsWith("http"):
      return Container.err(reversed_args)
    # If that's okay, but the second arg is NOT a url, that means it
    # is malformed.
    elif not parsed[1].startsWith("http"):
      return Container.err(malformed_args)
    # Otherwise, we are all good - pack shit up and get back to work
    else:
      var t: tuple[name, url: string]
      t.name = parsed[0]
      t.url = parsed[1]
      return Container.ok(t)

proc is_broken*(self: Container): bool =
  if self.isErr:
    case(self.error):
      of not_enough_args:
        echo "ERROR: Not enough arguments provided"
      of too_many_args:
        echo "ERROR: Too many arguments provided."
      of reversed_args:
        echo "ERROR: Did you reverse your args? Should be name then url"
      of malformed_args:
        echo "ERROR: No url provided, malformed args?"
    return true
  else:
    return false

