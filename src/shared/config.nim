import os
import json
import sequtils

type
  Manga* = object
    name: string
    url: string

  Configuration* = ref object
    storage*: string
    logpath*: string
    failjsonpath*: string
    manga: seq[Manga]
    delay*: int
    retries*: int
    retrydelay*: int

func name* (self: Manga): string {.inline.} = return self.name
func url* (self: Manga): string {.inline.} = return self.url
func newManga*(name, url: string): Manga =
  return Manga(name: name, url: url)

func manga* (self: Configuration): seq[Manga] {.inline.} = return self.manga

# TODO: remove the simulation check from this and do that from a separate function
proc dump*(self: Configuration, path: string, simulation: bool) =
  let data = %self
  
  if not simulation:
    if path.existsFile():
      echo "Creating backup of ", path, ": ", path, ".bak"
      moveFile(path, path & ".bak")
    let fh = open(path, fmWrite)
    fh.write(data.pretty())
    fh.close()

proc empty*(self: var Configuration) =
  self.manga = @[]

proc len*(self: Configuration): int {.inline.} = return self.manga.len

proc remove*(self: var Configuration, name, url: string) =
  var startlen = self.manga.len()
  self.manga.keepIf(
    proc(x: Manga): bool =
      var keep = false
      if name != "":
        if x.name != name:
          keep = true
        
      if url != "":
        if x.url != url:
          keep = true

      return keep
  )

  if startlen == self.manga.len():
    echo "No matches found - list not modified"
  else:
    echo "Removed entry that matches name='", name, "', url='", url, "'"

func removeByName*(self: var Configuration, name: string) =
  self.manga.keepIf(proc(x: Manga): bool = x.name != name)

func removeByUrl*(self: var Configuration, url: string) =
  self.manga.keepIf(proc(x: Manga): bool = x.url != url)

proc append*(self: Configuration, manga: Manga): bool =
  if manga in self.manga:
    echo "Manga already in database. Skipping"
    return false
  else:
    self.manga.add(manga)
#    echo "Added manga:"
#    echo "Name: ", manga.name
#    echo "Url: ", manga.url
    return true

#[
  Searches for configuration in this order:
    $CWD/localdir/default.json
    $XDG_CONFIG_HOME/mangaupdater/default.json
    $HOME/.mangaupdater/default.json
    /etc/mangaupdater/default.json (if linux)
]#

proc find_config*: string =
  result = getCurrentDir() / "default.json"
  if result.fileExists(): return

  result = getConfigDir() / "mangaupdater" / "default.json"
  if result.fileExists(): return

  result = getHomeDir() / ".mangaupdater" / "default.json"
  if result.fileExists(): return

  when defined unix:
    result = "/etc/mangaupdater/default.json"
    if result.fileExists(): return


proc load_configuration*(path: string): Configuration =
  let data = parseFile(path)
  try:
    result = to(data, Configuration)
  except KeyError as e:
    echo "Error parsing config:\n\t", e.msg
    quit(2)

proc dump_errors*(self: Configuration, path: string, failed: seq[Manga]) =
  var test = self.deepCopy()
  test.manga = failed
  var newJson = %*test

  assert(test != self)
  let failfp = open(path, fmWrite)
  defer: failfp.close()

  failfp.write(newJson.pretty())

