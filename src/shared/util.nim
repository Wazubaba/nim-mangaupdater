import os

# I can't believe we need this but here we go...
proc printf*(formatstr: cstring) {.importc: "printf", varargs,
    header: "<stdio.h>".}

# TODO: This should be moved to pathlib and renamed
proc log_prep*(path: string) =
  ## Grab the directory component of a provided path, ensure that path
  ## exists, and then continue on our merry way(s)
  let dir = path.splitPath()
  if dir.head == "":
    return
  else:
    os.createDir(dir.head)

# TODO: This should be moved to pathlib and renamed (maybe find_program_or_env?)
proc find_program*(name, env: string): string =
  result = name.findExe()
  if result.len <= 0:
    return os.getEnv(env)
  else:
    return result
