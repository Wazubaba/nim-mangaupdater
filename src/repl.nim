#import json
import strformat
import strutils
import parseopt
import os

import shared/config
import shared/util
import shared/parsing

const
  HELP_TEXT = staticRead("repl/helpfile")
  REPL_HELP_TEXT = staticRead("repl/replhelp")
  VERSION {.strdefine.}: string = ""

proc show_help(extval: int = 0) =
  let progname = paramStr(0)
  printf(HELP_TEXT, progname)
  quit(extval)

proc show_version =
  echo &"{paramStr(0)} - {VERSION}"
  quit(0)

proc do_save(data: Configuration, path: string, fakesave: bool) =
  if fakesave:
    echo "DEBUG: skipping save"
  else:
    echo "Saving data..."
  data.dump(path, fakesave)

proc do_download(data: Configuration, path: string, fakesave: bool): bool =
  # First backup the database
  data.do_save(path, fakesave)

  let updaterbin = find_program("updater", "MANGA_UPDATER")
  if updaterbin.len <= 0:
    echo "ERROR: Cannot find updater executable in path nor is MANGA_UPDATER environment variable set."
    return false
  else:
    echo "Launching updater with list: '", path, "'"
    let shellout = os.execShellCmd(&"{updaterbin} {path}")

    if shellout != 0:
      echo "ERROR: Failed to execute updater"
      return false
    else:
      return true

when isMainModule:
  var
    optparser = initOptParser()
    settings: tuple[
      simulation: bool,
      altpath: string,
      download: bool
    ]

  settings.simulation = false
  settings.altpath = ""
  settings.download = false

  for kind, key, val in optparser.getopt():
    case kind:
      of cmdArgument: discard
      of cmdLongOption, cmdShortOption:
        case key:
          of "help", "h": show_help()
          of "version", "v": show_version()
          of "simulation", "s": settings.simulation = true
          of "path", "p":
            if val.len <= 0:
              echo "Missing operand after path argument."
              show_help(1)
            else:
              settings.altpath = val
          of "download", "d": settings.download = true
      of cmdEnd: assert(false)
  
  let
    args = commandLineParams()

  if args.len() < 1:
    echo "Missing path to config."
    show_help(1)

  var data = load_configuration(args[0])
  var outputpath = if settings.altpath.len > 0: settings.altpath else: args[0]

  echo "Welcome to mangaupdater repl!"
  echo "Enter a name and a url separated by a space and hit enter."
  echo "Enter help for usage informations."
  while true:
    write(stdout, "> ")
    let input = stdin.readLine()
    if input.toLower() in ["q", "qu", "qui", "quit"]:
      break

    elif input.toLower() in ["s", "sa", "sav", "save"]:
      data.do_save(outputpath, settings.simulation)

    elif input.toLower() in ["h", "he", "hel", "help"]:
      echo REPL_HELP_TEXT

    elif input.toLower() in ["d", "do", "dow", "down", "downl", "downlo", "downloa", "download"]:
      # WHY HAVE I NOT AUTOMATED THIS VIA A FUNCTION YET REEEEEEEEEEEEEEEEEEEEEEEEEE
      discard data.do_download(outputpath, settings.simulation)

    elif input.toLower() in ["b", "ba", "bas", "base"]:
      echo "Please enter a path to base from (leave blank to cancel)"
      write(stdout, "\tbase-path: ")
      let newBase = stdin.readLine()
      if newBase.len <= 0: continue

      if not newBase.fileExists():
        echo "ERROR: new base path '", newBase, "' does not exist"
        continue
      write(stdout, "Save current list to '", outputpath, "'? [Y/n/o]: ")
      case stdin.readLine().toLower():
        of "o":
          echo "Leave blank to cancel"
          write(stdout, "\tsave path: ")
          let otherfile = stdin.readLine()
          if otherfile.len <= 0: continue
          echo "Leave blank to cancel"
          write(stdout, "\tUpdate path to '", otherfile, "'? [Y/n]: ")
          if stdin.readLine().toLower() != "n":
            outputpath = otherfile

          log_prep(otherFile)
          data.do_save(otherFile, false) # Force this save
        of "n":
          discard
        else:
          data.do_save(outputpath, settings.simulation) # This one is different though

      data = load_configuration(newBase)
      data.empty()
      echo "Rebased current database to match '", newBase, "'"
    
    elif input.toLower() in ["i", "in", "inf", "info"]:
      echo "Using database file: '", outputpath, "'"
      echo "Currently have ", data.len(), " manga entries."

    else:
      let opts = split_name_url(input)
      if opts.is_broken:
        echo "Type 'help' for usage information or 'quit' to exit"
        continue

      else:
        echo "Adding new manga:"
        echo "\tName: ", opts.value.name
        echo "\tUrl: ", opts.value.url

        let newManga = newManga(opts.value.name, opts.value.url)
        if not data.append(newManga):
          echo "ERROR: Failed to append new manga"
    echo ""

  data.do_save(outputpath, settings.simulation)

  if settings.download:
    discard data.do_download(outputpath, settings.simulation)

