import os
import strformat
import strutils
import parseopt

import shared/config as settings
import shared/util

const VERSION {.strdefine.}: string = ""

when defined debug:
  echo ".:DEBUG MODE ENABLED:."
  import random

# We can't do this at compile time because windows is gay and has to have
# user-based fucking temp dirs fucking windows
let
  TEMPDIR = when defined(unix): "/tmp" else: string(getEnv("TEMP"))

# Inject the helpfile contents
const HELP_TEXT = staticRead("updater/helpfile")

proc download(config: Configuration, item: Manga): int =
  ## Attempt to download the current target item.
  ## Returns 0 if no issue, 1 if critical issue, and 2 if download
  ## failed somehow (couldn't download to the tmppath or something)
  let
    url = item.url#.sanitize_url()
    root = config.storage#.sanitize_path()
    tmpdir = joinPath(TEMPDIR, "mangaupdater")
    tmppath = joinPath(tmpdir, "INCOMING")
    target = joinPath(root, item.name)

  # Ensure our manga dir actually exists before trying to download
  os.createDir(root)
  os.createDir(tmpdir)

  if not os.dirExists(root):
    echo "ERROR: Failed to find storage directory"
    return 1
  if not os.dirExists(tmpdir):
    echo "ERROR: Failed to create temporary download directory"
    return 1

  # This is kinda shitty, but due to the way mangaupdater works this
  # is necessary. First, test if we already have this manga in storage,
  # and then if so create a symlink to it in TEMPDIR/mangaupdater/INCOMING.
  # If we *don't* have it, then just skip this step
  var madeSymlink = false
  if os.dirExists(target):
    echo "Found existing copy of '", item.name, "' in storage"
    echo "Checking for updates..."
    os.createSymlink(target, tmppath)
    madeSymlink = true
  else:
    echo "Downloading new manga: '", item.name, "'..."

  # Prep to run the command
  when defined debug:
    let dbg_prefix = "echo DEBUG: COMMAND="
  else:
    let dbg_prefix = ""

  if execShellCmd(&"{dbg_prefix}manga-py --rename-pages -d {tmpdir} -n INCOMING {url}") > 0:
    echo "ERROR: Failed to invoke manga-py"
    if madeSymlink: os.removeFile(tmppath)
    return 1

  # Check to make sure we actually downloaded the bloody file things
  when defined debug:
    # Use the debug method of testing since we don't actually download
    # anything
    if not sample([true, true, true, true, false]):
      echo "DEBUG: ERROR: Failed to download"
      if madeSymlink: os.removeFile(tmppath)
      return 2
  else:
    if not os.dirExists(tmppath):
      echo "ERROR: Failed to download"
      if madeSymlink: os.removeFile(tmppath)
      return 2

  # I hate to repeat myself this fucking much wrt symlink cleanup but
  # atm I can't think of a better way. Will probably try to improve this
  # later somehow...
  #? Perhaps try wrapping this in a container function that handles
  #? cleanup afterwards and propogates the result up the stack?
  if madeSymlink:
    # If we made a symlink, clean it up just in case (nim docs say it
    # could fail otherwise)
    os.removeFile(tmppath)
    echo "Update complete"
  else:
    # Otherwise we need to copy the new manga over...
    os.moveDir(tmppath, target)
    echo "Download successful"

  when not defined debug: echo ""
  return 0


proc show_help =
  printf(HELP_TEXT, paramStr(0))
  quit(0)

proc show_version =
  echo &"{paramStr(0)} - {VERSION}"
  quit(0)

when isMainModule:
  var
    optparser = initOptParser()
    failed: seq[Manga]
    config_path: string
    delay, retries: int = -1
    storage, logpath: string

  # Handle argument-overrides
  for kind, key, val in optparser.getopt():
    case kind:
      of cmdArgument:
        config_path = key
      of cmdLongOption, cmdShortOption:
        case key:
          of "help", "h": show_help()
          of "version", "v": show_version()
          of "path", "p": config_path = val
          of "storage", "s": storage = val
          of "logpath", "l": logpath = val
          of "delay", "d": delay = parseInt(val)
          of "retries", "r": retries = parseInt(val)
      of cmdEnd: assert(false) # should never happen afaict from the docs


  # If no config is specified, fallback to the default ones
  if config_path.len() < 1: config_path = find_config()

  let
    config = load_configuration(config_path)

  # Apply the overridden values
  if storage.len() > 0:
    config.storage = storage
  if logpath.len() > 0:
    config.logpath = logpath
  if delay > -1:
    config.delay = delay
  if retries > -1:
    config.retries = retries

  when defined debug:
    echo "Configuration:"
    echo "\tusing config: ", config_path
    echo "\tstorage: ", config.storage
    echo "\tlogpath: ", config.logpath
    echo "\tdelay: ", config.delay
    echo "\tretries: ", config.retries

  var
    abort = false

  for element in 0..<config.manga.len():
    let current = config.manga[element]
    var tries = 0
    while true:
      let check = download(config, current)
      if check == 1:
        echo "Critical failure detected, aborting downloads"
        abort = true
        break
      elif check == 2:
        tries.inc()
        if tries > config.retries:
          echo "Download failed."
          echo &"Maximum retries for {current.url} exceeded, skipping..."
          failed.add(current)
          break
        else:
          echo &"Download failed - re-attempt {tries}... in {config.retrydelay}ms"
          os.sleep(config.retrydelay)
      else:
        break

    if abort:
      echo "Please fix the reported issues and try again"
      quit(1)

    if element == config.manga.len - 1:
      echo "All manga synchronized."
      break
    else:
      os.sleep(config.delay)

  if failed.len > 0:
    # Ensure the logging paths all exist properly
    logprep(config.logpath)
    logprep(config.failjsonpath)

    echo &"Some manga failed to download. Logs written to {config.logpath}"
    let logfile = open(config.logpath, fmWrite)
    defer: logfile.close()
    for manga in failed:
      logfile.writeLine(manga.name)
      logfile.writeLine(manga.url)
      logfile.writeLine("")

    echo &"Dumping condensed json of failed downloads to {config.failjsonpath}"
    config.dump_errors(config.failjsonpath, failed)


