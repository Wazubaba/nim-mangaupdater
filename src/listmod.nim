#import json
import strformat
#import strutils
import os

#import result

import shared/config
import shared/util
#import shared/parsing

const
  HELP_TEXT = staticRead("listmod/helpfile")
  VERSION {.strdefine.}: string = ""

proc show_help(extval: int = 0) =
  let progname = paramStr(0)
  printf(HELP_TEXT, progname, progname, progname, progname, progname)
  quit(extval)

proc show_version =
  echo &"{paramStr(0)} - {VERSION}"
  quit(0)

when isMainModule:
  let
    args = commandLineParams()

  if args.len() < 2:
    echo "Missing arguments."
    show_help(1)

  if "--help" in args or "-h" in args:
    show_help()

  if "--version" in args or "-v" in args:
    show_version()

  let
    operation = args[1]
    name = if args.len() > 2: args[2] else: ""
    url = if args.len() > 3: args[3] else: ""

  var data = load_configuration(args[0])

  if operation == "add" and url == "":
    echo "Missing url argument for add procedure"
    quit(3)
  
  var simulation = true
  case operation:
    of "tadd":
      echo "-=SIMULATION=-"
      let newManga = newManga(name, url)
      if not data.append(newManga):
        quit(4)
      simulation = true
    of "tdel":
      echo "-=SIMULATION=-"
      data.remove(name, url)
      simulation = true
    of "add":
      let newManga = newManga(name, url)
      if not data.append(newManga):
        quit(4)
      simulation = false
    of "del":
      data.remove(name, url)
      simulation = false
    else:
      echo "Invalid operation: ", operation
      quit(2)

  data.dump(args[0], simulation)

