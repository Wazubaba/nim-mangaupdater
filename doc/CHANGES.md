## INPROGRESS: v0.4.0
* Get rid of nake dependency and port build system completely over to
nimscript.

* Bundle manga-py because of stupid breakages. Is now a git submodule.

* Find my dumbass and remember how to program because depression is a hell
of a drug.

* Fix issues due to the `result` library having a minor api change.

*	Rename mangaupdater to updater and update all build scripts.

* Remove old example config and replace with an empty skeleton with
proper documentation of each key in `doc/USAGE.md`.

* Cleanup nakefile a bit to be more coherent.

* Add info function to repl to get some stats about the currently
selected database file.

* Separate repl into it's own binary, and overhaul it.

* Try to give updater windows support.

* Add --version command to listmod.

* Update versions infos
	+ project 0.1.0 -> 0.4.0
	+ updater 1.0.0 -> 1.2.0
	+ listmod 0.5.0 -> 0.6.0

* Rework build to place all executables within a bin directory.

* FINALLY SOLVE THE FUCKING PATHS ISSUES.

* Add preliminary information with usage on Windows.

* Improve error messages to be more coherent and understandable
(hopefully).

* Overhaul download routine and simplify.

	+ Merged test_download functionality and enhanced the tests.

	+ Make downloads go to /tmp/mangaupdater/INCOMING instead so as to
	always provide a path manga-py can handle since it seems to choke on
	any and all special chars regardless of quotation or escaping...

	+ Intelligently symlink an existing manga in place so as to still
	take advantage of manga-py's update features if necessary.

	+ Implement checks to ensure if a critical failure occurs then
	rather than sitting around trying to download updater will abort
	instead.

	+ Cleanup download loop and all relevant testing it performs.

* Add a changes file to keep track of recent changes separate from git
	..now I just need to remember to update this fucking file when I do
	bulk updates...

*	Move help files into subdirs pertaining to their specific bin.

* Move config to shared directory for all future code that is shared
(duh).

* Implement support to dump a json config with the same settings as
your current config but only containing the manga that failed to
download (makes fixing this stuff easier tbqh). If a bug report is
ever submitted this file could be useful in debugging the issue.

* Add new configuration options (which are still mandatory for now).

* Add functionality to create paths for logs if necessary so there
won't be a crash if the directories don't already exist.

* Update readme with proper name of updater.

* Add result dependency.

* Re-add pathlib dependency. Some of the functions in src/shared/util.nim
should be moved over to there...

* Fix sleep call after all manga are finished being synchronized.

* Possibly other things that I missed, check the git log if necessary...
