## listmod
implement a clean (or prune maybe a better name?) function which
can run a url-based check to see if there are duplicates, and then
offer a selection screen akin to what nimble does for configuration
generation to choose which entry to keep and which ones to discard.

Also overhaul the informational output and add some kind of safety
test to parsing the manga names and urls so as to not clobber the
database with broken and/or malformed data.

Add support to only download newly-added manga.

Implement that command expansion function we need.


sync - update everything (what download does now)
pull - get only the newly-added manga for now

## Repl
rather than subproc things, perhaps consider wrapping their functionality
into `when isMainModule:` blocks and then importing them into repl. The
only problem with this is that in a proper installation repl should *always*
be able to find updater and this would bloat repl to be fairly large.

## Configuration
Consider implementing a tags array of strings for each manga which
can be used to generate or work with a tmsu database for easy sorting.
Also it would be nice if we could figure out a way to handle fetching
a manga's cover image (perhaps see if there's a site that has them?),
since we'll already know the manga's name anyways.

# Codebase
Do some refactoring and cleaning up a bit. Make shit more coherent pl0x...

