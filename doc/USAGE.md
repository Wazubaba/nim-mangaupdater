## Usage
`mangaupdater` is a collection of tools that are used to ease collecting,
mangaging, and downloading manga from various sources.
*	`updater` - Used for downloading manga from your list
*	`listmod` - A swiss-army-knife for mangaging your list
* `repl`    - A tool to give you an interactive repl for adding
	entries to your list

All included programs have documentation via the `--help` argument.

## Configuration
For `mangaupdater` a configuration file contains both run-time configuration
for the various tools and your list of manga. When you use any tool in
the `mangaupdater` suite just about every one of them will take the path
to your config as the first argument.

A skeleton config is shipped in `etc/skeleton.json`. All values you see
in this file **must** be included in yours, save for the manga list
being able to contain as many manga as you want (just about). Order
does not matter, so long as you ensure to keep the correct syntax.
It is for this reason precisely that `listmod` and `repl` exist :)

I suggest copying `etc/skeleton.json` to your own file and altering the
settings to your liking, referencing the tables below for info on what
each key does. Alternatively, you can use `repl` and `base` off the
`etc/skeleton.json` file for a new one. (Provided this functionality
is actually working despite my making it after three nights without sleep).
(Make backups people...)

## Keys and their meanings

### Normal keys
| key          | type   | description
|:------------:|--------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
|    storage   | string | Path to where to store the downloaded manga.                                                                                                             |
|    logpath   | string | Path to where to put logs should manga fail to download within the configured number of retries.                                                         |
| failjsonpath | string | Path to where to place the condensed config with only the mangas that failed to download for future re-attempts.                                         |
|    retries   | number | Number of times to re-attempt a download should it fail.                                                                                                 |
|     delay    | number | Delay in milliseconds (1000 = 1 second) between downloads. This is used so as to not flood manga hosting site's servers and get yourselves in trouble :P |
|  retrydelay  | number | Delay in milliseconds between retries should a download fail. See delay for further information.                                                         |
|     manga    | list   | List of manga to download. See manga keys table below.                                                                                                        |


### Manga keys
| key  | type   | description
|------|--------|----------------------------------------------------------------------------------------------------------|
| name | string | Name of the manga (This will be the name of the folder it is saved to as well)                           |
| url  | string | Link to where the manga is (You might need to reference manga-py's docs or play a bit to get this right) |

