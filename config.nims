import strutils
import os

const
  MUVERSION* = "1.2.0"
  LMVERSION* = "0.6.0"
  RPVERSION* = "0.5.0"
  DOC_DIR* = "doc"/"generated"
  DEPENDENCIES* = [
    "result"
  ]


when defined(linux):
  const CACHEDIR* = ".nimcache"/"linux"
elif defined(unix):
  const CACHEDIR* = ".nimcache"/"unix"
elif defined(windows):
  const CACHEDIR* = ".nimcache"/"windows"
else:
  const CACHEDIR* = ".nimcache"/"idkwtf"

func build_command*(s: varargs[string, `$`]): string =
  for arg in s:
    result &= " " & arg
  result.strip()

proc build_debug*(name, main, version: string) =
  let
    aVersion = "--define:VERSION:" & version & "-debug"
    aCache = "--nimcache:" & CACHEDIR & "_d"
    aBinout = "--out:bin"/name
    aDebug = "--debuginfo --define:debug"

  selfExec(build_command("c", aDebug, aVersion, aCache, aBinout,
    main))

proc build_release*(name, main, version: string) =
  let
    aVersion = "--define:VERSION:" & version
    aCache = "--nimcache:" & CACHEDIR & "_r"
    aBinout = "--out:bin"/name
    aRelease = "--define:release --opt:size --define:danger"

  selfExec(build_command("c", aRelease, aVersion, aCache, aBinout, main))

  let stripbin = system.findExe("strip")
  if stripbin.len > 0:
    exec(build_command("strip", "--strip-unneeded", "bin"/name))

proc build_docs*(main: string) =
  let
    aOut = "--out:" & DOC_DIR
  selfExec(build_command("doc2", "--project", aOut, main))

proc showmsg*(m: string) =
  echo("*".repeat(m.len+2))
  echo("*" & m & "*")
  echo("*".repeat(m.len+2))

task setup_mangapy, "Install manga-py and it's dependencies for the local user":
  withDir("thirdparty"):
    withDir("manga-py"):
      showmsg("Installing manga-py dependencies via pip for local user")
      exec("pip install --user -r requirements.txt")
      showmsg("Installing manga-py for local user")
      exec("python setup.py install --user")

task depfix, "Prep git submodules and install dependencies":

  showmsg("Installing nim dependencies via nimble")
  for dep in DEPENDENCIES:
    exec("nimble install " & dep)

  showmsg("Fetching manga-py")
  exec("git submodule update --init --recursive")
  selfExec("setup_mangapy")

task dbg_listmod, "Build listmod in debug mode":
  build_debug("listmod-debug", "src"/"listmod.nim", LMVERSION)

task dbg_repl, "Build repl in debug mode":
  build_debug("repl-debug", "src"/"repl.nim", RPVERSION)

task dbg_updater, "Build updater in debug mode":
  build_debug("updater-debug", "src"/"updater.nim", MUVERSION)

task debug, "Build all components in debug mode":
  selfExec("dbg_listmod")
  selfExec("dbg_repl")
  selfExec("dbg_updater")

task rel_listmod, "Build listmod in release mode":
  build_release("listmod", "src"/"listmod.nim", LMVERSION)

task rel_repl, "Build repl in release mode":
  build_release("repl", "src"/"repl.nim", RPVERSION)

task rel_updater, "Build updater in release mode":
  build_release("updater", "src"/"updater.nim", MUVERSION)

task release, "Build all components in release mode":
  selfExec("rel_listmod")
  selfExec("rel_repl")
  selfExec("rel_updater")

task docs, "Build documentation from source files":
  build_docs("src"/"listmod.nim")
  build_docs("src"/"updater.nim")
  build_docs("src"/"repl.nim")

task clean, "Clean generated files":
  rmDir("bin")
  rmDir(".nimcache")

task clean_docs, "Purge generated documentation files":
  rmDir(DOC_DIR)

task dist_clean, "Restore to a fresh working directory":
  selfExec("clean")
  selfExec("clean-docs")

task build, "Generic build for users":
  selfExec("depfix")
  showmsg("Building all components in release mode with optimizations enabled.")
  selfExec("release")
  showmsg("Optimized release build complete")
